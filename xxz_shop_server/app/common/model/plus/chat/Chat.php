<?php


namespace app\common\model\plus\chat;

use app\common\model\BaseModel;

/**
 * 客服消息模型
 */
class Chat extends BaseModel
{
    protected $pk = 'chat_id';
    protected $name = 'chat';

    /**
     * 关联会员表
     */
    public function user()
    {
        return $this->hasOne("app\\common\\model\\user\\User", 'user_id', 'user_id');
    }

    /**
     * 关联会员表
     */
    public function supplier()
    {
        return $this->hasOne("app\\common\\model\\supplier\\Supplier", 'shop_supplier_id', 'shop_supplier_id');
    }

    //获取聊天验证id
    public function getIdentify($user_id, $muser_id)
    {
        if ($user_id > $muser_id) {
            $identify = $user_id . '_' . $muser_id;
        } else {
            $identify = $muser_id . '_' . $user_id;
        }
        return $identify;
    }

    //添加信息
    public function add($data)
    {
        // 开启事务
        $this->startTrans();
        try {
            if ($data['from_id'] == $data['to']) {
                $this->error = '不允许跟自己聊天';
                return false;
            }
            $ChatRelation = new ChatRelation();
            $identify = $this->getIdentify($data['from_id'], $data['to']);
            $data['identify'] = $identify;
            $data['user_id'] = $data['from_id'];
            $this->save($data);
            $info = $ChatRelation->where('user_id', '=', $data['from_id'])->where('muser_id', '=', $data['to'])->find();
            if (!$info) {
                $relation = [
                    ['user_id' => $data['from_id'], 'muser_id' => $data['to'], 'app_id' => $data['app_id']],
                    ['user_id' => $data['to'], 'muser_id' => $data['from_id'], 'app_id' => $data['app_id']]
                ];
                $ChatRelation->saveAll($relation);
            }
            $this->commit();
            return true;
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
            $this->rollback();
            return false;
        }

    }
}