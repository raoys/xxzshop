<?php

namespace app\supplier\model\product;

use app\common\enum\settings\OperateTypeEnum;
use app\common\library\helper;
use app\common\model\product\Product as ProductModel;
use app\common\model\settings\Setting;
use app\shop\service\ProductService;
use app\supplier\model\supplier\Supplier as SupplierModel;
/**
 * 商品模型
 */
class Product extends ProductModel
{
    /**
     * 添加商品
     */
    public function add(array $data)
    {   
        
        if (!isset($data['image']) || empty($data['image'])) {
            $this->error = '请上传商品图片';
            return false;
        }
        $data['content'] = isset($data['content']) ? $data['content'] : '';
        $data['app_id'] = $data['sku']['app_id'] = self::$app_id;

        $this->processContent($data);
        // 开启事务
        $this->startTrans();
        try {
            // 添加商品
            $this->save($data);
            // 商品规格
            $this->addProductSpec($data);
            // 商品图片
            $this->addProductImages($data['image'], $data['shop_supplier_id']);
            $this->commit();
            return true;
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
            $this->rollback();
            return false;
        }
    }

    /**
     * 添加商品图片
     */
    private function addProductImages($images, $shop_supplier_id)
    {
        $this->image()->delete();
        $data = array_map(function ($images)  use ($shop_supplier_id)  {
            isset($images['file_id']) && $image_id = $images['file_id'];
            isset($images['image_id']) && $image_id = $images['image_id'];
            if(!isset($images['file_id']) && !isset($images['image_id'])){
                $image_id = $images['file_path'];
            }
            // 如果是外部链接
            if(strpos($image_id,"http") > -1){
                $imageService = new ImageService($image_id, $shop_supplier_id);
                $model = $imageService->replaceRemoteImage();
                if($model){
                    $image_id = $model['file_id'];
                }else{
                    $image_id = 0;
                }
            }
            return [
                'image_id' => $image_id,
                'app_id' => self::$app_id
            ];
        }, $images);
        return $this->image()->saveAll($data);
    }

    /**
     * 编辑商品
     */
    public function edit($data,$shop_supplier_id)
    {   
        
        if (!isset($data['image']) || empty($data['image'])) {
            $this->error = '请上传商品图片';
            return false;
        }
        $data['spec_type'] = isset($data['spec_type']) ? $data['spec_type'] : $this['spec_type'];
        $data['content'] = isset($data['content']) ? $data['content'] : '';
        $data['app_id'] = $data['sku']['app_id'] = self::$app_id;
        $productSkuIdList = helper::getArrayColumn(($this['sku']), 'product_sku_id');
        // 商品状态，如果已审核过的，看平台配置是否需要再次审核
        return $this->transaction(function () use ($data, $productSkuIdList) {
            // 如果审核未通过，修改后再次审核
            if($this['audit_status'] == 20){
                $data['product_status'] = 40;
                $data['audit_status'] = 0;
            }
            // 保存商品
            $this->save($data);
            // 商品规格
            $this->addProductSpec($data, true, $productSkuIdList);
            // 商品图片
            $this->addProductImages($data['image'], $this['shop_supplier_id']);
            return true;
        });
    }

    /**
     * 添加商品规格
     */
    private function addProductSpec($data, $isUpdate = false, $productSkuIdList = [])
    {
        // 更新模式: 先删除所有规格
        $model = new ProductSku;
        $isUpdate && $model->removeAll($this['product_id']);
        $stock = 0;//总库存
        $supplier_price = 0;//价格
        $product_price = 0;
        $line_price = 0;
        // 商城设置
        $settings = Setting::getItem('store');
        // 添加规格数据
        if ($data['spec_type'] == '10') {
            // 删除多规格遗留数据
            $isUpdate && $model->removeSkuBySpec($this['product_id']);
            // 单规格
            // 添加的时候填充价格
            $data['sku']['product_price'] = $data['sku']['supplier_price'];
            $this->sku()->save($data['sku']);
            $stock = $data['sku']['stock_num'];
            $supplier_price = $data['sku']['supplier_price'];
            $line_price = $data['sku']['line_price'];
        } else if ($data['spec_type'] == '20') {
            // 添加商品与规格关系记录
            $model->addProductSpecRel($this['product_id'], $data['spec_many']['spec_attr']);
            // 添加商品sku
            $model->addSkuList($this['product_id'], $data['spec_many']['spec_list'], $productSkuIdList);
            $supplier_price = $data['spec_many']['spec_list'][0]['spec_form']['supplier_price'];
            foreach ($data['spec_many']['spec_list'] as &$item) {
                $stock += $item['spec_form']['stock_num'];
                if($item['spec_form']['supplier_price'] < $supplier_price){
                    $supplier_price = $item['spec_form']['supplier_price'];
                }
                if($item['spec_form']['line_price'] < $line_price){
                    $line_price = $item['spec_form']['line_price'];
                }
            }
        }
        $save_data = [
            'product_stock' => $stock,
            'supplier_price' => $supplier_price,
            'line_price' => $line_price
        ];
        // 商品价格
        $save_data['product_price'] = $supplier_price;
        // 修改后是否需要审核
        if($isUpdate && $settings['edit_audit'] == 1){
            $save_data['product_status'] = 40;
            $save_data['audit_status'] = 0;
        }
        $this->save($save_data);
    }

    /**
     * 修改商品状态
     */
    public function setStatus($state)
    {
        return $this->save(['product_status' => $state == 10 ? 20 : 10]) !== false;
    }

    /**
     * 软删除
     */
    public function setDelete()
    {
        if (ProductService::checkSpecLocked($this, 'delete')) {
            $this->error = '当前商品正在参与其他活动，不允许删除';
            return false;
        }
        return $this->save(['is_delete' => 1]);
    }

    /**
     * 获取当前商品总数
     */
    public function getProductTotal($where = [])
    {
        return $this->where('is_delete', '=', 0)->where($where)->count();
    }

    /**
     * 获取商品告急数量总数
     */
    public function getProductStockTotal($shop_supplier_id)
    {
        return $this->where('is_delete', '=', 0)->where('product_stock', '<', 10)
            ->where('shop_supplier_id', '=', $shop_supplier_id)
            ->count();
    }

    /**
     * 处理内容
     */
    private function processContent(&$data){
        $pattern = "/src=[\"\'](.*?)[\"\']/is";
        preg_match_all($pattern, $data['content'], $match);
    }

    /**
     * 组件列表
     */
    public function getProductList($shop_supplier_id, $params){
        return $this->with(['image.file'])->where('shop_supplier_id', '=', $shop_supplier_id)
            ->where('audit_status', '=', 10)
            ->where('product_status', '=', 10)
            ->where('is_delete', '=', 0)
            ->paginate($params, false, [
                'query' => \request()->request()
            ]);
    }

    /**
     * 获取数量
     */
    public function getCount($type , $shop_supplier_id){
        $model = $this;
        //已下架
        if ($type == 'lower') {
            $model = $model->whereRaw('audit_status=30 or product_status=20');
        }
        //待审核
        if ($type == 'audit') {
            $model = $model->where('audit_status', '=', 0);
            $model = $model->where('product_status', '=', 40);
        }
        //未通过
        if ($type == 'no_audit') {
            $model = $model->where('audit_status', '=', 20);
        }
        //库存紧张
        if ($type == 'stock') {
            $model = $model->whereBetween('product_stock', [1, 20]);
            $model = $model->where('product_status', '=', 10);
            $model = $model->where('audit_status', '=', 10);
        }
        //已售罄
        if ($type == 'over') {
            $model = $model->where('product_stock', '=', 0);
            $model = $model->where('product_status', '=', 10);
            $model = $model->where('audit_status', '=', 10);
        }
        if($shop_supplier_id != 0){
            $model = $model->where('shop_supplier_id', '=', $shop_supplier_id);
        }
        return $model->where('is_delete', '=', 0)
            ->count();
    }
}
