<?php

namespace app\api\controller;

use app\api\model\page\Page as AppPage;
use app\api\model\settings\Setting as SettingModel;
use app\common\model\app\AppUpdate as AppUpdateModel;
use app\common\model\supplier\Service as ServiceModel;
/**
 * 页面控制器
 */
class Index extends Controller
{
    /**
     * 首页
     */
    public function index($page_id = null)
    {
        // 页面元素
        $data = AppPage::getPageData($this->getUser(false), $page_id);
        $data['setting'] = array(
            'collection' => SettingModel::getItem('collection'),
            'officia' => SettingModel::getItem('officia'),
            'homepush' => SettingModel::getItem('homepush'),
        );
        return $this->renderSuccess('', $data);
    }

    // 公众号客服
    public function mpService($shop_supplier_id)
    {
        $mp_service = ServiceModel::detail($shop_supplier_id);
        return $this->renderSuccess('', compact('mp_service'));
    }
    //底部导航
    public function nav(){ 
       $data['nav'] = SettingModel::getItem('nav');
       $data['bottomnav'] = SettingModel::getItem('bottomnav');
       return $this->renderSuccess('', $data); 
    }

    // app更新
    public function update($name, $version, $platform){
        $result = [
            'update' => false,
            'wgtUrl' => '',
            'pkgUrl' => '',
        ];
        try {
            $model = AppUpdateModel::getLast();
            // 这里简单判定下，不相等就是有更新。
            if($model && $version != $model['version']){
                $currentVersions = explode('.', $version);
                $resultVersions = explode('.', $model['version']);

                if ($currentVersions[0] < $resultVersions[0]) {
                    // 说明有大版本更新
                    $result['update'] = true;
                    $result['pkgUrl'] = $platform == 'android'?$model['pkg_url_android']:$model['pkg_url_ios'];
                } else {
                    // 其它情况均认为是小版本更新
                    $result['update'] = true;
                    $result['wgtUrl'] = $model['wgt_url'];
                }
            }
        } catch (\Exception $e) {

        }
        return $this->renderSuccess('', compact('result'));
    }
}
