<?php

namespace app\api\controller\supplier;

use app\api\controller\Controller;
use app\api\model\product\Product as ProductModel;
use app\api\model\supplier\Supplier as SupplierModel;
/**
 * 供应商产品
 */
class Product extends Controller
{
    
    /**
     * 供应商中心主页
     */
    public function index()
    { 
        $data = $this->postData();
        $user = $this->getUser();
        // 获取商品列表数据
        $model = new ProductModel;
        $productList = $model->getList($data, $user);
        return $this->renderSuccess('', compact('productList'));
    }
    //商品上下架
    public function modify(){
        $data = $this->postData();
        // 获取商品数据
        $model = ProductModel::detail($data['product_id']);
        if(!$model->editStatus($data)){
            return $this->renderError('操作失败');
        }
        return $this->renderSuccess('操作成功');
    }
   
}