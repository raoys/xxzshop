<?php

namespace app\api\controller\supplier;

use app\api\controller\Controller;
use app\api\model\product\Product as ProductModel;
use app\api\model\supplier\DepositOrder as DepositOrderModel;
use app\api\model\supplier\Supplier as SupplierModel;
use app\api\model\plus\coupon\Coupon as CouponModel;
use app\api\model\page\Ad as AdModel;
use app\api\model\order\Order as OrderModel;
use app\api\model\user\Visit as VisitModel;
use app\common\service\statistics\OrderService;
use app\supplier\service\statistics\UserService;
use app\supplier\model\order\OrderSettled as OrderSettledModel;
use app\api\model\settings\Setting as SettingModel;
use app\api\model\plus\live\Room as RoomModel;
use app\common\model\supplier\Service as ServiceModel;
/**
 * 供应商
 */
class Index extends Controller
{
    //店铺列表
    public function list(){
        $param = $this->postData();
        $SupplierModel = new SupplierModel;
        $list = $SupplierModel->supplierList($param);
        return $this->renderSuccess('', compact('list'));
    }
    /**
     * 供应商中心主页
     */
    public function index()
    { 
        $data = $this->postData();
        $supplier = new SupplierModel;
        $user = $this->getUser(false);
        //获取店铺信息
        $detail = $supplier->getDetail($data, $user);
        if(!$detail){
            return $this->renderError('店铺不存在');
        }
        //banner图
        $AdModel = new AdModel;
        $adList = $AdModel->getIndex(['shop_supplier_id'=>$data['shop_supplier_id']],5); 
        //优惠券
        $dataCoupon['shop_supplier_id'] = $data['shop_supplier_id'];
        $model = new CouponModel;
        $couponList = $model->getWaitList($dataCoupon, $user,1);
        // 访问记录
        (new VisitModel())->addVisit($user, $detail, $data['visitcode'], null);
        //直播列表
        $model = new RoomModel();
        $liveList = $model->getStoreList($this->postData());
        //是否显示直播
        $liv_status = SettingModel::getItem('live');
        //是否开启客服
        $service_open = SettingModel::getSysConfig()['service_open'];
        //店铺客服信息
        $mp_service = ServiceModel::detail($data['shop_supplier_id']);
        return $this->renderSuccess('', compact('detail', 'couponList','adList','liveList','liv_status','service_open','mp_service'));
    }
     //成交数据
    public function tradeData(){
        $data = $this->postData();
        $user = $this->getUser();
        $data['shop_supplier_id'] = $user['supplierUser']['shop_supplier_id'];
        if(!$data['shop_supplier_id'] > 0){
            return $this->renderError('您还未开通店铺');
        }
        $is_open = SettingModel::getItem('live')['is_open'];
        //累积成交笔数
        $totalCount = OrderModel::getTotalPayOrder($data['shop_supplier_id']);
        //今日成交笔数
        $todayCount = OrderModel::getTodayPayOrder($data['shop_supplier_id']);
        //累积领取
        $supplier = SupplierModel::detail($data['shop_supplier_id']);
        return $this->renderSuccess('', compact('totalCount','todayCount' ,'supplier','is_open'));
    }

    /**
     * 付押金
     */
    public function deposit(){
        // 用户信息
        $user = $this->getUser();
        $supplier = SupplierModel::detail($user['supplierUser']['shop_supplier_id'], ['category']);
        // 类目
        $category = $supplier['category'];
        if($this->request->isGet()){
            // 返回结算信息
            return $this->renderSuccess('', compact('category'));
        }
        $params = $this->request->param();

        // 生成订单
        $model = new DepositOrderModel;
        $order_id = $model->createOrder($user,$supplier,$params['pay_type'],$params['pay_source']);
        if(!$order_id){
            return $this->renderError($model->getError() ?: '购买失败');
        }
        // 在线支付
        $payment = DepositOrderModel::onOrderPayment($user, $model, $params['pay_type'], $params['pay_source']);
        // 返回结算信息
        return $this->renderSuccess(['success' => '支付成功', 'error' => '订单未支付'], [
            'order_id' => [$order_id],   // 订单id
            'pay_type' => $params['pay_type'],  // 支付方式
            'payment' => $payment,               // 微信支付参数
        ]);
    }

    //店铺数据
    public function storedata(){
        $user = $this->getUser();
        //成交量
        $order = (new OrderService($user['supplierUser']['shop_supplier_id']))->getData();
         // 访问量
        $visit = (new UserService($user['supplierUser']['shop_supplier_id']))->getData();
        //订单结算
        $ordersettle = (new OrderSettledModel())->getList($user['supplierUser']['shop_supplier_id'], $this->postData());
        return $this->renderSuccess('', compact('order' ,'visit','ordersettle'));
    }
    /**
     * 详情
     */
    public function settledetail($settled_id){
        $model = OrderSettledModel::detail($settled_id);
        $order = OrderModel::detail($model['order_id']);
        return $this->renderSuccess('', compact('model', 'order'));
    }
}