<?php

namespace app\api\model\plus\chat;
use app\common\model\plus\chat\Chat as ChatModel;
use app\api\model\supplier\Supplier as SupplierModel;
use app\api\model\user\User as UserModel;
use app\api\model\settings\Setting as SettingModel;
/**
 * 客服消息模型类
 */
class Chat extends ChatModel
{
    private $token;

    /**
     * 隐藏字段
     */
    protected $hidden = [
        'app_id',
        'identify',
        'status',
        'update_time'
    ];
    //消息列表
    public function myList($user){
        $ChatRelation = new ChatRelation();
        $list = $ChatRelation->alias('u')
            ->where(['u.user_id'=>$user['user_id']])
            ->join('xxzshop_user ju','ju.user_id=u.muser_id')
            ->field('ju.user_id,nickName,avatarUrl')
            ->select();
        foreach ($list as $key => &$value) {
            $identify = $this->getIdentify($value['user_id'],$user['user_id']);
      
            $where['user_id'] = $value['user_id'];
            $where['status'] = 0;
            $where['identify'] = $identify;

            $supplierInfo = SupplierModel::where('user_id','=',$value['user_id'])->with(['logo'])->field('name,logo_id,shop_supplier_id')->find();
            if($supplierInfo['logo']['file_path']){
                $value['avatarUrl'] = $supplierInfo['logo']['file_path'];
            }
            if($supplierInfo){
                $value['nickName'] = $supplierInfo['name'];
                $value['shop_supplier_id'] = $supplierInfo['shop_supplier_id'];
            }
            
            $value['num'] = $this->where($where)->count();
            unset($where['status']);unset($where['user_id']);
            $value['newMessage'] = $this->where($where)->order('chat_id desc')->field('content,create_time,type')->find();
        }
        return $list;
    }
   
    //获取聊天信息
    public function getMessage($data,$user){
        $identify = $this->getIdentify($user['user_id'],$data['user_id']);
        $where['identify'] = $identify;  
        $list = $this->where($where)
                        ->with(['user','supplier.logo'])
                        ->order('chat_id desc')
                        ->paginate($data, false, [
                                'query' => \request()->request(),
                            ]);
        $where['user_id'] = $data['user_id'];
        $this->where($where)->update(['status'=>1]);
        return $list;
    }
    //获取消息条数
    public function mCount($user){
      
        $num = 0;
        if($user){
            $where[] = ['user_id','<>',$user['user_id']];
            $where[] = ['status','=',0];
            $where[] = ['identify','like','%'.$user['user_id'].'%'];
            $num = $this->where($where)->count(); 
        }
        return $num;
    }
    //获取用户信息
    public function getInfo($data){
        $userInfo = UserModel::detail($data['user_id']);
        $supplierInfo = SupplierModel::detail($data['shop_supplier_id'], ['logo']);
        $data['avatarUrl'] = $userInfo['avatarUrl'];
        $data['logo'] = $supplierInfo['logo']['file_path'];
        $data['url'] = SettingModel::getSysConfig()['url'];
        return $data ;
    }
    
}
